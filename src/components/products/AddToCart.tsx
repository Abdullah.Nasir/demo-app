import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../store/cart";
import { RootState } from "../../store/types";

const AddToCart = ({ productId }: { productId: string }) => {
	const dispatch = useDispatch();

	const addToCartHandler = () => {
		dispatch(addToCart(productId));
	};

	const cartQty = useSelector((state: RootState) => {
		const productCartState = state.cart.cart[productId];

		if (productCartState) {
			return productCartState.qty;
		} else {
			return 0;
		}
	});

	return (
		<button onClick={addToCartHandler} className="border border-gray p-2 text-blue-300 mt-2 cursor-pointer hover:border-blue-500">
			{!cartQty && (
				<>
					<img src="plus_icon.png" alt="plus" className="inline mb-1 mr-2" height="16" width="16" />
					Add to Cart
				</>
			)}
			{cartQty > 0 && (
				<div className="flex justify-between pl-4 pr-4">
					<div>{cartQty}</div>
					<div>Add more</div>
				</div>
			)}
		</button>
	);
};

export default AddToCart;
