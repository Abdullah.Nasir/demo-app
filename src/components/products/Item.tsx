import AddToCart from "./AddToCart";

const Product = ({
	product,
}: {
	product: {
		name: string;
		imageUrl: string;
		unitType: string;
		price: number;
		productId: { value: string };
	};
}) => {
	return (
		<div className="w-1/4 mt-12 flex flex-col border-box">
			<div className="h-3/4">
				<img src={product.imageUrl} alt="product" className="object-contain h-full w-2/4 mx-auto" />
			</div>
			<div className="text-center pl-8 pr-8 h-1/4 pt-14 flex flex-col justify-end">
				<div className="text-sm leading-relaxed">{product.name}</div>
				<div className="text-md mt-1">${product.price}</div>
				<AddToCart productId={product.productId.value} />
			</div>
		</div>
	);
};

export default Product;
