import { useSelector } from "react-redux";
import { getProductsByCategory } from "../../store/products";
import Product from "./Item";

const ProductsList = () => {
	let products = useSelector(getProductsByCategory);

	return (
		<div className="flex flex-wrap">
			{products?.map(product => (
				<Product product={product} />
			))}
		</div>
	);
};

export default ProductsList;
