const IconWithText = ({ img, text }: { img: string; text: string }) => {
	return (
		<div className="flex flex-col w-16 justify-center items-center">
			<div>
				<img className="object-contain" width="24px" height="24px" src={img} alt="" />
			</div>
			<div>{text}</div>
		</div>
	);
};

export default IconWithText;
