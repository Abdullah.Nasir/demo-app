const CategoryItem = ({ category, isSelected, onSelection }: { category: string; isSelected: boolean; onSelection: Function }) => {
	return (
		<>
			<button
				onClick={() => {
					onSelection(category);
				}}
				className={`border border-grey-100 rounded-full p-2 pl-4 pr-4 h-10 mr-2 ${isSelected ? "bg-gray-100" : ""}`}>
				{category}
			</button>
		</>
	);
};

export default CategoryItem;
