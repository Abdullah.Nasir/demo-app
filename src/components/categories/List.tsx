import { useDispatch, useSelector } from "react-redux";
import { getCategories, getSelectedCategory, setCategorySelection } from "../../store/categories";
import CategoryItem from "./Item";

const CategoriesList = () => {
	const dispatch = useDispatch();

	const categories = useSelector(getCategories);
	const selectedCategory = useSelector(getSelectedCategory);

	const handleCategorySelection = (selectedCategory: string) => {
		dispatch(setCategorySelection(selectedCategory));
	};

	return (
		<>
			<div className="mb-3 text-sm font-meduium">Shop by cateogry</div>
			{categories.map((cateogry: string) => (
				<CategoryItem onSelection={handleCategorySelection} isSelected={selectedCategory === cateogry} category={cateogry} />
			))}
		</>
	);
};

export default CategoriesList;
