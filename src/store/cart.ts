import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import { RootState } from "./types";

type cartState = {
	cart: {
		[key: string]: {
			qty: number;
		};
	};
};

const initialState: cartState = { cart: {} };

const CartSlice = createSlice({
	initialState,
	name: "cart",
	reducers: {
		addToCart: (state, action: PayloadAction<string>) => {
			const productId = action.payload;

			if (!state.cart[productId]) {
				state.cart[productId] = {
					qty: 1,
				};
			} else {
				state.cart[productId].qty += 1;
			}
		},
	},
});

// ACTIONS
export const { addToCart } = CartSlice.actions;

// SELECTORS
export const getCart = createSelector(
	(state: RootState) => state.cart.cart,
	cart => cart
);

export default CartSlice.reducer;
