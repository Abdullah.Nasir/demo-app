import { createSlice, createSelector } from "@reduxjs/toolkit";
import { getSelectedCategory } from "./categories";
import products from "../data/products.json";
import { RootState } from "./types";

const productsSlice = createSlice({
	initialState: {
		allProducts: products,
	},
	name: "categories",
	reducers: {},
});

// SELECTORS
export const getProducts = (state: RootState) => state.product.allProducts;
export const getProductsByCategory = createSelector(getProducts, getSelectedCategory, (products, selectedCategory) => {
	let filteredProducts = [];
	if (selectedCategory) {
		filteredProducts = products.filter(product => product.category === selectedCategory);
	} else {
		filteredProducts = products;
	}

	// just returning 50 products so that it doesn't effect page scroll speed
	return filteredProducts.slice(0, 50);
});

// ACTIONS
// export const { setCategorySelection } = productsSlice.actions;

export default productsSlice.reducer;
