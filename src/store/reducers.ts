import { combineReducers } from "@reduxjs/toolkit";
import categoryReducer from "./categories";
import productReducer from "./products";
import cartReducer from "./cart";

const rootReducer = combineReducers({
	category: categoryReducer,
	product: productReducer,
	cart: cartReducer,
});

export default rootReducer;
