import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "./types";

const categoriesSlice = createSlice({
	initialState: {
		categories: ["Alcohol", "Bakery", "Dairy & Eggs", "Drinks", "Frozen", "Home & Health"],
		selectedCategory: "",
	},
	name: "categories",
	reducers: {
		setCategorySelection: (state, action) => {
			state.selectedCategory = action.payload;
		},
	},
});

// SELECTORS
export const getCategories = (state: RootState) => state.category.categories;
export const getSelectedCategory = (state: RootState) => state.category.selectedCategory;

// ACTIONS
export const { setCategorySelection } = categoriesSlice.actions;

export default categoriesSlice.reducer;
