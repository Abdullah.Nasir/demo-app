import "./App.css";
import CategoriesList from "./components/categories/List";
import IconWithText from "./components/IconWithText";
import ProductsList from "./components/products/List";

function App() {
	return (
		<>
			<div className="w-100 p-4 flex">
				<div className="flex w-2/3">
					<img className="h-8 ml-2 self-center" src="logo.png" alt="logo" />
					<input
						className="pl-6 ml-8 h-12 border border-gray-200 rounded outline-gray-200 focus:outline-none focus:border-gray-400 flex-grow self-center"
						placeholder="Search site"
						type="text"></input>
				</div>
				<div className="flex flex-grow justify-end items-center">
					<button className="w-32 h-10 bg-red-500 text-white rounded mr-3">Get $20 Off</button>
					<IconWithText img="recipes.svg" text="Recipes" />
					<IconWithText img="shop.svg" text="Shop" />
					<IconWithText img="profile_pic.png" text="Profile" />
					<IconWithText img="settings.svg" text="Settings" />
				</div>
			</div>
			<div className="container mx-auto">
				<CategoriesList />
				<div className="mt-4"></div>
				<ProductsList />
			</div>
		</>
	);
}

export default App;
